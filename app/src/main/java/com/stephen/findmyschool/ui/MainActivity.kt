// Created by Stephen R Ouellette for POC exploration with CITI and TCS
package com.stephen.findmyschool.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.stephen.findmyschool.R
import com.stephen.findmyschool.data.api.ApiHelper
import com.stephen.findmyschool.data.api.ApiServiceBuilder
import com.stephen.findmyschool.data.model.PrimaryViewModel
import com.stephen.findmyschool.data.model.School
import com.stephen.findmyschool.utilities.Status
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), OnSchoolItemClickListener {

    private lateinit var vm: PrimaryViewModel
    private lateinit var adapter: Adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViewModel()
        setupObserver()
        setupRecyclerView()


    }

    private fun setupObserver() {
        vm.fetchSchools().observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        recyclerView.isVisible = true
                        progressBar.isVisible = false
                        resource.data?.let { schools -> addListToAdapter(schools) }
                    }
                    Status.ERROR -> {
                        recyclerView.isVisible = true
                        progressBar.isVisible = false
                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                        progressBar.isVisible = true
                        recyclerView.isVisible = false
                    }
                }
            }
        })
    }

    private fun addListToAdapter(schools: List<School>) {
        adapter.apply {
            addList(schools)
        }
    }

    private fun setupRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = Adapter(arrayListOf(), arrayListOf(), this)
        recyclerView.addItemDecoration(
                DividerItemDecoration(
                        recyclerView.context,
                        (recyclerView.layoutManager as LinearLayoutManager).orientation
                )
        )
        recyclerView.setOnClickListener { }
        recyclerView.adapter = adapter
        registerForContextMenu(recyclerView)
    }

    private fun setupViewModel() {
        vm = ViewModelProviders.of(
                this,
                ViewModelFactory(ApiHelper(ApiServiceBuilder.apiService), this))
                .get(PrimaryViewModel::class.java)
    }

    //Handles click event for recycler view items
    override fun onSchoolItemClick(item: School) {
        val fragMan = supportFragmentManager.beginTransaction()
        val frag = FragmentSATResult(item)
        fragMan.replace(R.id.container, frag).addToBackStack("main")
        showRecycler(false)
        showIcons(false)
        fragMan.commit()
    }

    //Handles back button click
    override fun onBackPressed() {
        super.onBackPressed()
        showRecycler(true)
        showIcons(true)
    }

    private fun showRecycler(show: Boolean) {
        recyclerView.isVisible = show
    }

    private fun showIcons(show: Boolean) {
        val searchIcon: View = findViewById(R.id.search)
        val sortIcon: View = findViewById(R.id.sort)

        searchIcon.isVisible = show
        sortIcon.isVisible = show
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.menu, menu)
        val menuSearch = menu!!.findItem(R.id.search)

        if (menuSearch != null) {
            val searchView = menuSearch.actionView as SearchView

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {

                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {

                    if (newText!!.isNotEmpty()) {

                        adapter.clearDisplayList()
                        val search = newText.toLowerCase(Locale.getDefault())

                        adapter.getList(false).forEach {
                            if (it.schoolName.toLowerCase(Locale.getDefault()).contains(search)) {
                                adapter.addToDisplayList(it)
                            }
                        }
                    } else {
                        adapter.addList(adapter.getList(false))
                        adapter.notifyDataSetChanged()
                    }

                    adapter.notifyDataSetChanged()
                    return true
                }

            })
        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.dbnSort -> adapter.sortList("dbn")
            R.id.nameSort -> adapter.sortList("name")
            R.id.citySort -> adapter.sortList("city")
            R.id.studentCountSort -> adapter.sortList("count")
        }
        item.isChecked = true
        return super.onOptionsItemSelected(item)
    }

}