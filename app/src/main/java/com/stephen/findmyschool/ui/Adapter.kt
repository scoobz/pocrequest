package com.stephen.findmyschool.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.stephen.findmyschool.R
import com.stephen.findmyschool.data.model.School
import kotlinx.android.synthetic.main.school_item.view.*


// This class provides our bridge between the data from API and the recycler view object in our view
class Adapter(
        private val schoolList: ArrayList<School>,
        private val displayList: ArrayList<School>,
        private var clickListener: OnSchoolItemClickListener?)
    : RecyclerView.Adapter<Adapter.SchoolViewHolder>() {

    //this class allows the adapter to use this object as a view holder.
    inner class SchoolViewHolder(itemView: View, listener: OnSchoolItemClickListener?) : RecyclerView.ViewHolder(itemView) {
        init {
            listener?.let { click ->
                itemView.setOnClickListener {
                    click.onSchoolItemClick(schoolList[adapterPosition])
                }
                //itemView.setOnClickListener { listener?.onSchoolItemClick(schoolList[adapterPosition]) }
            }
        }
        //Function to bind the data from the individual items to the recycler view list item.
        fun bind(school: School) {
            itemView.tv_name.text = school.schoolName
            itemView.tv_city.text = school.city
            itemView.tv_count.text = school.totalStudents
            itemView.tv_dbn.text = school.dbn
        }
    }

    //onCreateViewHolder creates a new holder for our display items, then they are reused as needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
                R.layout.school_item, parent, false
        )
        return SchoolViewHolder(itemView, clickListener)
    }

    //binding the data of the item at the given position to the view
    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        if (position < displayList.size) {
            clickListener?.let { holder.bind(displayList[position]) }
                    //using the elvis we can ensure we still bind without click listener...
                    ?: holder.bind(displayList[position])
        }
    }

    // returns the length of the school list so we know how many to iterate over
    override fun getItemCount() = displayList.size

    //addList function takes in a list of schools as a param, and then if the lists are empty
    // which indicates this is the first time were running this function, we add the full list
    // to the display and school lists. if the list is not empty, we then just update
    // the display list.
    fun addList(schools: List<School>) {
        if (this.schoolList.isEmpty()) {
            this.schoolList.apply { addAll(schools) }
            this.displayList.apply { addAll(schools) }
        } else {
            this.displayList.apply {
                clear()
                addAll(schools)
            }
        }
    }

    //addToDisplayList is used to add a single school object to the display list. used when
    //performing a search on the school list.
    fun addToDisplayList(school: School) {
        this.displayList.apply {
            add(school)
        }
    }

    //getList returns the list depending on the boolean passed...
    //if you pass false, then you will get the school list...
    //if you pass true, then you will get the display list...
    fun getList(isDisplayList: Boolean): List<School> {
        return if (isDisplayList) this.displayList else this.schoolList
    }

    //clearDisplayList provides the functionality to clear the display list
    //we use this prior to applying the search to our list.
    fun clearDisplayList() {
        displayList.clear()
    }

    //sortList provides the functionality to take a string and then sort the list depending on
    //the string passed. we use the selected menu item and a switch statement to provide the string
    //then we sort by that request...
    fun sortList(sort: String) {
        this.displayList.sortBy {
            when (sort) {
                "name" -> it.schoolName
                "city" -> it.city
                "dbn" -> it.dbn
                "count" -> it.totalStudents
                else -> {
                    it.schoolName
                }
            }
        }
        notifyDataSetChanged()
    }
}

//This interface is how we communicate with the main activity.
interface OnSchoolItemClickListener {
    fun onSchoolItemClick(item: School)
}