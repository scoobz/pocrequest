package com.stephen.findmyschool.ui

import android.app.Activity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.stephen.findmyschool.data.api.ApiHelper
import com.stephen.findmyschool.data.model.PrimaryViewModel
import com.stephen.findmyschool.data.repo.Repo

class ViewModelFactory(private val helper: ApiHelper, private val activity: Activity) : ViewModelProvider.Factory {
    //this warning is natural consequence of type erasure. it's advised to use @SuppressWarnings("unchecked")
    //is there a better way?
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PrimaryViewModel::class.java)) {
            return PrimaryViewModel(Repo(helper), activity.application) as T
        }
        throw IllegalArgumentException("Unknown class provided...")
    }
}