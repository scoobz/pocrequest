package com.stephen.findmyschool.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import com.stephen.findmyschool.R
import com.stephen.findmyschool.data.api.ApiServiceBuilder
import com.stephen.findmyschool.data.model.SATResult
import com.stephen.findmyschool.data.model.School
import kotlinx.android.synthetic.main.fragment_sat_results.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentSATResult(private val school: School): Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        populateData()

        return inflater.inflate(R.layout.fragment_sat_results, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true){
            override fun handleOnBackPressed() {
                fragmentManager?.popBackStack()
            }
        })
    }

    private fun populateData() {
        val call by lazy {ApiServiceBuilder.apiService.fetchSATResults(school.dbn)}
        call.enqueue(object: Callback<List<SATResult>>{
            override fun onResponse(call: Call<List<SATResult>>, response: Response<List<SATResult>>) {
                if(response.isSuccessful){
                    val responseList = response.body()
                    if(responseList != null){
                        bindDataToView()
                        if(responseList.isNotEmpty()) {
                            bindResultsToView(responseList[0])
                        }
                    }
                }
            }

            override fun onFailure(call: Call<List<SATResult>>, t: Throwable) {
                t.message?.let { Log.i("Fragment SAT Result", it) }
            }
        })
    }

    private fun bindResultsToView(data: SATResult){
        card_sat_results.setScores(
                data.satMathAvgScore,
                data.satWritingAvgScore,
                data.satCriticalReadingAvgScore)
    }

    private fun bindDataToView(){
        tv_detailsSchoolName.text = school.schoolName
        tv_detailsSchoolStudentCount.text = school.totalStudents
        tv_detailsSchoolAddress.text = school.getAddress()
        tv_detailsSchoolEmail.text = school.schoolEmail
        tv_detailsSchoolPhone.text = school.phoneNumber
    }
}