package com.stephen.findmyschool.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.stephen.findmyschool.R
import kotlinx.android.synthetic.main.card_sat_results.view.*


//This custom view extends the Linear Layout class
//Were using card_sat_results.xml for reference
class CustomCardView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context)
                .inflate(R.layout.card_sat_results, this, true)
        orientation = VERTICAL

        //if the attributes isn't null
        attrs?.let {
            //create a typed array of the card View styleable
            val typedArray = context.obtainStyledAttributes(it, R.styleable.CustomCardView, 0, 0)
            //set the labels
            val mathLabel = resources.getText(typedArray.getResourceId(R.styleable.CustomCardView_mathLabel, R.string.math_label))
            val readingLabel = resources.getText(typedArray.getResourceId(R.styleable.CustomCardView_mathLabel, R.string.reading_label))
            val writingLabel = resources.getText(typedArray.getResourceId(R.styleable.CustomCardView_mathLabel, R.string.writing_label))
            val mathScore = resources.getText(typedArray.getResourceId(R.styleable.CustomCardView_mathScore, R.string.placeholder_score))
            val writingScore = resources.getText(typedArray.getResourceId(R.styleable.CustomCardView_mathScore, R.string.placeholder_score))
            val readingScore = resources.getText(typedArray.getResourceId(R.styleable.CustomCardView_mathScore, R.string.placeholder_score))
            custom_card_math_label.text = mathLabel
            custom_card_reading_label.text = readingLabel
            custom_card_writing_label.text = writingLabel

            //preset the scores (this sets them to No Score) in case a school doesn't have a score.
            custom_card_math_score.text = mathScore
            custom_card_reading_score.text = readingScore
            custom_card_writing_score.text = writingScore

            typedArray.recycle()

        }
    }

    //function to set the text values of the reading math and writing scores
    // requires Math, Writing, Reading strings in this order.
    fun setScores(math: String, writing: String, reading: String){
        custom_card_math_score.text = math
        custom_card_writing_score.text = writing
        custom_card_reading_score.text = reading
        postInvalidate()
    }
}