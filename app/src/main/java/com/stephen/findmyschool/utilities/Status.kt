package com.stephen.findmyschool.utilities

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}