package com.stephen.findmyschool.utilities

data class Resource<out T>(val status: Status, val data: T?, val message: String?){
    companion object {
        //Handles success
        fun <T> success(data: T): Resource<T> = Resource(
            status = Status.SUCCESS, data = data, message = null)
        //Handles Loading
        fun <T> loading(data: T?): Resource<T> = Resource(
            status = Status.LOADING, data = data, message = null)
        //Handles Error
        fun <T> error(data: T?, message: String): Resource<T> = Resource(
            status = Status.ERROR, data = data, message = message)

    }
}
