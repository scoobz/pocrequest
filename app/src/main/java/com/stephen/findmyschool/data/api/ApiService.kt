package com.stephen.findmyschool.data.api

import com.stephen.findmyschool.data.model.SATResult
import com.stephen.findmyschool.data.model.School
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

// Provides the ability to make requests and get responses for retrofit.
// we use @GET to append onto the base url...
interface ApiService {

    //this "get" is appended onto the end of the BASE URL
    //we can also use query here to narrow things down...
    @GET("uq7m-95z8.json")
    suspend fun fetchSchools() : List<School>

    @GET("f9bf-2cp4.json")
    fun fetchSATResults(@Query("dbn") dbn: String): Call<List<SATResult>>

}