package com.stephen.findmyschool.data.model

import com.google.gson.annotations.SerializedName

// Model for the school object we get returned from the api calls
data class School(
        @SerializedName("school_name")
        val schoolName: String,
        @SerializedName("phone_number")
        val phoneNumber: String,
        @SerializedName("primary_address_line_1")
        val primaryAddress: String,
        @SerializedName("state_code")
        private val state: String,
        val city: String,
        private val zip: String,
        @SerializedName("school_email")
        val schoolEmail: String,
        @SerializedName("total_students")
        val totalStudents: String,
        val dbn: String) {

    //this function is an address builder.
    fun getAddress(): String {
        val sb = StringBuilder()
        sb.append("$primaryAddress ")
                .append("$city, ")
                .append("$state. ")
                .append(zip)

        return sb.toString()
    }

}