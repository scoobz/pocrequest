package com.stephen.findmyschool.data.model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.liveData
import com.stephen.findmyschool.R
import com.stephen.findmyschool.data.repo.Repo
import com.stephen.findmyschool.utilities.Resource
import kotlinx.coroutines.Dispatchers.IO

// The main purpose of this class is to create a single object for the view to render,
// this reduces the requirement of complex ui logic code.
// here were creating our Coroutine which handles the live data for the fetchSchools function
// emit is how we send updates to the coroutine.
class PrimaryViewModel(private val repo: Repo, app: Application): AndroidViewModel(app) {

    //Coroutine dispatcher...
    fun fetchSchools() = liveData(IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = repo.fetchSchools()))
        } catch(ex: Exception){
            emit(Resource.error(data = null, message = ex.message ?: getApplication<Application>().applicationContext.getString(R.string.emitError)))
        }
    }
}