package com.stephen.findmyschool.data.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


// here were creating our service, which is an object that has all the information we need
// we create the client which is okHttp which retrofit uses to make the call
// we add in our converter in this case the Gson converter, and the base url
// then we pass in our interface and then we get our full retrofit object
// this will contain the object needed for our call.
object ApiServiceBuilder {

    //BASE URL
    private const val BASE_URL = "https://data.cityofnewyork.us/resource/"

    //Build a logger
    //private val logger = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

    //Build the okHttp Client for logging
    //private val okHttp = OkHttpClient.Builder().addInterceptor(logger)

    //Retrofit - reduces boiler plate, provides compatibility with coroutines
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            //.client(okHttp.build())
            .build()
    }

    val apiService: ApiService = getRetrofit().create(ApiService::class.java)
}