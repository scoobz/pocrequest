package com.stephen.findmyschool.data.repo

import com.stephen.findmyschool.data.api.ApiHelper

// Repository class helps us isolate the data source from the rest of the application
// this ensures a clean API for data access.
class Repo(private val helper: ApiHelper) {
    //Function to call the data access layer helper which gives us a list of School objects.
    suspend fun fetchSchools() = helper.fetchSchools()
}