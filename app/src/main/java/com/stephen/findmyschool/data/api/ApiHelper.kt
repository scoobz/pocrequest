package com.stephen.findmyschool.data.api

// Data access layer between the repository layer and the service layer
class ApiHelper(private val service: ApiService) {
    // Calls the function from the service that returns a list of school objects...
    suspend fun fetchSchools() = service.fetchSchools()
}